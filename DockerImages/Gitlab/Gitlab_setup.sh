#get the image from docker
#docker pull ajhave5/gitlab

# create a container from the image and run it. Exposed ports are 30080 and 30022 on the host machine
docker run --detach --name gitlab \
	--hostname gitlab.example.com \
	--publish 30080:30080 \
    --publish 30022:22 \
	--env GITLAB_OMNIBUS_CONFIG="external_url 'http://gitlab.example.com:30080'; gitlab_rails['gitlab_shell_ssh_port']=30022;" \
	gitlab/gitlab-ce:9.1.0-ce.0
	
#transfer the tar files inside the container and extract it in the appropriate directory.
docker cp ./DockerImages/Gitlab/my-gitlab_log.tgz gitlab:/var/log/
docker cp ./DockerImages/Gitlab/my-gitlab_opt.tgz gitlab:/var/opt/
docker cp ./DockerImages/Gitlab/my-gitlab_etc-gitlab.tgz gitlab:/etc/gitlab/
echo "Gitlab tars copied"

#Execute commands inside the container
docker exec gitlab /bin/sh -c "cd /var/log;tar xvzf my-gitlab_log.tgz; rm my-gitlab_log.tgz; cd /var/opt/;tar xvzf my-gitlab_opt.tgz;rm my-gitlab_opt.tgz;cd /etc/gitlab/;tar xvzf my-gitlab_etc-gitlab.tgz; rm my-gitlab_etc-gitlab.tgz"
echo "Gitlab tars extracted and deleted tars"

#restart gitlab
docker restart gitlab
