#get the image from docker
#docker pull ajhave5/jenkins

# create a container from the image and run it. Exposed ports are 9080 and 50000 on the host machine
docker run --detach --name jenkins  -p 9080:8080 -p 50000:50000 ajhave5/jenkins:latest
echo "Jenkins image downloaded & container running"
	
#transfer the tar files inside the container and extract it in the appropriate directory.
docker cp ./DockerImages/Jenkins/my-jenkins_home.tgz jenkins:/var/jenkins_home/
echo "Jenkins tar copied"

#Execute the commands inside the Jenkins container
docker exec -it jenkins bash -c "cd /var/jenkins_home/; tar xvzf my-jenkins_home.tgz"
echo "Jenkins tar extracted"

#restart jenkins
docker stop jenkins
sleep 3
docker start jenkins
echo "Jenkins restarted"